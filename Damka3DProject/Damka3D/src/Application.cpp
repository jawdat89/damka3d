/* This is the console executable, that makes use of the Damka class
This acts as the view in a MVC pattern, and is responsible for all
user interaction. For game logic see the Damka class.
*/
// OpenGL dependencies 
#include <GL/glew.h>
#include <GLFW/glfw3.h>
// cpp code libraries
#include <iostream>
#include <string>
//OpenGL Abstaction
#include "Renderer.h"
#include "VertexBuffer.h"
#include "VertexBufferLayout.h"
#include "IndexBuffer.h"
#include "VertexArray.h"
#include "Shader.h"
#include "Texture.h"
//Blending
/* OpenGL Mathematics (GLM): https:/glm.g-truc.net - https:/github.com/g-truc/glm */
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtx/io.hpp"
// imgui
#include "imgui/imgui.h"
#include "imgui/imgui_impl_glfw_gl3.h"


// TODO: create Damka class 

//functions 
//TODO: PrintIntro();
//TODO: PlayGame();
//TODO: GetPossibles();
//TODO: AskToPlayAgain();
//TODO: PrintGameSummary();

int InitGame();

bool PlayGame();

int main()
{
    bool bPlayAgain = false;
    bool bGameInitiated = false;
    do //TODO: make Play Agian functioning
    {
        // PrintIntro
        std::cout << "Damka3D Game\n"
            << "By: Jawdat Abdullah and Ronny Moadi\n"
            << std::endl; // TODO: Change to Print Intro
            // PlayGame
        bGameInitiated = PlayGame();
        if (bGameInitiated) // initializing OpenGL error occured
        {
            std::cout << "OpenGL Error Occured!!!" << std::endl;
            return -1;
        }
        // bPlayAgain = AskToPlayAgain();
    } while (bPlayAgain);
    // PrintGameSummary();
    return 0; // exit the application
}

//Starting the Game
bool PlayGame()
{
    /* Initialize OpenGL */
    if (InitGame())
        return false;
    /* Cleanup */
    //imgui
    ImGui_ImplGlfwGL3_Shutdown();
    ImGui::DestroyContext();
    //glfw
    glfwTerminate(); // terminate OpenGL
    return true;
}

int InitGame()
{
    GLFWwindow* window;

    /* Initialize the library */
    if (!glfwInit())
        return -1;

    // Tell OpenGL i want to create context with OpenGL Core Profile
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(1280, 960, "Damka3D", NULL, NULL);  // 4 : 3 Asspect ratio
    if (!window)
    {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // limiting our pixels
    glfwSwapInterval(1);

    /* Initialize GLEW library */
    if (glewInit() != GLEW_OK)
        std::cout << "GLEW Error!" << std::endl;

    std::cout << "OpenGL version: " << glGetString(GL_VERSION) << std::endl;

    {
        /*glViewport(0, 0, width(), height());*/ //TODO: delete or make it work
        float positions[] = { // Index 
            -0.5f, -0.5f, 0.0f, 0.0f, // 0
             0.5f, -0.5f, 1.0f, 0.0f, // 1
             0.5f,  0.5f, 1.0f, 1.0f, // 2
            -0.5f,  0.5f, 0.0f, 1.0f  // 3
        };

        unsigned int indices[] = { // sent to GPU to draw square ( Index Buffer )
            0, 1, 2,
            2, 3, 0
        };

        // Enabling Blending
        GLCall(glEnable(GL_BLEND));
        GLCall(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));

        //Create Vertex Array buffer
        VertexArray va;

        //Give OpenGL the Data
        VertexBuffer vb(positions, 4 * 4 * sizeof(float)); // n of vertex * m of float

        //Add A Layout to VertexArray buffer
        VertexBufferLayout layout;
        layout.Push<float>(2);  // 2 deminition
        layout.Push<float>(2);  // 2 texture coord
        va.AddBuffer(vb, layout);

        //Tell the GPU what to draw
        IndexBuffer ib(indices, 6);
        /* Projector matrix */
        glm::mat4 proj = glm::ortho(-2.0f, 2.0f, -1.5f, 1.5f, -1.0f, 1.0f); // 4 x 3 View 
        glm::mat4 view = glm::translate(glm::mat4(1.0f), glm::vec3(0, 0, 0)); // position of Camera

        //Inializing Shader       
        Shader shader("res/shaders/Basic.shader");
        shader.Bind();
        shader.SetUniform4f("u_Color", 0.8f, 0.3f, 0.8f, 1.0f);
        
        //Import Texture
        Texture texture("res/textures/CPP.png");
        texture.Bind(); // default --> slot = 0
        shader.SetUniform1i("u_Texture", 0); // 0 --> slot

        //Unbind
        va.Unbind();
        vb.Unbind();
        ib.Unbind();
        shader.Unbind();

        // imgui initialization 
        ImGui::CreateContext();
        ImGui_ImplGlfwGL3_Init(window, true);
        //imgui style
        ImGui::StyleColorsDark();

        Renderer renderer;

        glm::vec3 translataion(0.5, 0.5, 0); // mvp throw imgui

        float r = 0.0f;
        float increment = 0.05f;
        /* Loop until the user closes the window */
        while (!glfwWindowShouldClose(window))
        {
            /* Render here */
            renderer.Clear();

            ImGui_ImplGlfwGL3_NewFrame(); // start a new imgui frame 
            // implementing live mvp
            glm::mat4 model = glm::translate(glm::mat4(1.0f), translataion);
            glm::mat4 mvp = proj * view * model; // result on screen

            shader.Bind(); // bind shader
            shader.SetUniform4f("u_Color", r, 0.3f, 0.8f, 1.0f); // set the Uniform
            shader.SetUniformMat4f("u_MVP", mvp); // provide math to shaders

            renderer.Draw(va, ib, shader);

            if (r > 1.0f)
                increment = -0.05f;
            else if (r < 0.0f)
                increment = 0.05f;

            r += increment;

            //imgui calculation
            {
                ImGui::SliderFloat3("Translataion", &translataion.x, 0.0f, 1.0f);
                ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
            }

            // render imgui
            ImGui::Render();
            ImGui_ImplGlfwGL3_RenderDrawData(ImGui::GetDrawData());


            /* Swap front and back buffers */
            glfwSwapBuffers(window);

            /* Poll for and process events */
            glfwPollEvents();
        }
    }
    return 0;
}