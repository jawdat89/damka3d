#shader vertex
#version 330 core

layout(location = 0) in vec4 position;
layout(location = 1) in vec2 texCoord;

out vec2 v_TexCoord; // getting out to fragment shader

uniform mat4 u_MVP; // taking uniform of matix from CPU to the shader

void main()
{
    gl_Position = u_MVP * position; // move to aproperiate space based on the mathmatics provided
    v_TexCoord = texCoord;
};

#shader fragment
#version 330 core

layout(location = 0) out vec4 color;

in vec2 v_TexCoord; // getting in from vertex shader

uniform vec4 u_Color;
uniform sampler2D u_Texture;

void main()
{
    vec4 texColor = texture(u_Texture, v_TexCoord);
    color = u_Color;
};
